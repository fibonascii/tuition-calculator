tuition = 8000
total = 0.0
rate = 0.3


def attending_school():
    print("The first year of tuition will be ${}".format(tuition))
    question = input('Will you be be attending next year?: ')

    return question
    
def calculate_tuition():
    year = int(input("Enter numerically how many years you will attend: "))
    for counter in range(0, year):
        total = year * tuition + rate 
    print('The total is ${}'.format(total))

    
def additional_questions():
    another_question = input("Did you have something else to calculate: ")

    return another_question

def main():

    while True:
        if attending_school() == 'y':
            calculate_tuition()
        else:
            print("Invalid response. Please try again")
            continue    

        if additional_questions() == 'y':
            continue 
        elif additional_questions() == 'n' or 'N':
            break 
        else: 
            break    


if __name__ == '__main__':
    main()
            
    





